import React, { useState, useEffect, useRef } from "react";
import "./restaurant-card.scss";
import Collapse from "@material-ui/core/Collapse";
import RestaurantInfo from "../restaurant-info/restaurant-info.jsx";

export default function RestaurantCard(props) {
  // expanded will tell material UI to open the collapse or not
  const [expanded, setExpanded] = useState(false);
  // This ref is used to scroll to the top of the div if clicked on
  const cardRef = useRef(null);

  // This part is a little inefficient basically each component checks
  // to see if it has been clicked on to decide to close the collapsable
  // if it is collapsable, setExpanded state and remove the eventListener
  const offSelfClick = e => {
    if (!cardRef.current.contains(e.target)) {
      setExpanded(!expanded);
      window.removeEventListener("click", offSelfClick);
    }
  };

  // check if expanded is changed to scroll to top and set event
  // listener if expanded is true to allow us to click off the item
  // and scroll to the top of the item
  useEffect(() => {
    if (expanded) {
      window.scrollTo(0, cardRef.current.offsetTop);
      window.addEventListener("click", offSelfClick);
    }
  }, [expanded]);

  // Toggles the expanded property given a click on the top level component
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <div ref={cardRef} className="restaurant-card">
      <div onClick={handleExpandClick} className="restaurant-top">
        <img className="restaurant-image" src={props.imgSrc} alt={props.name} />
        <img
          className="restaurant-gradient"
          src={require("../media/cellGradientBackground@2x.png")}
          alt="gradient-overlay"
        />
        <div className="restaurant-details">
          <div className="restaurant-name">{props.name}</div>
          <div className="restaurant-category">{props.category}</div>
        </div>
      </div>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <RestaurantInfo
          name={props.name}
          category={props.category}
          index={props.index}
          location={props.location}
          contact={props.contact}
        />
      </Collapse>
    </div>
  );
}
