import React from "react";
import GoogleMapComponent from "./google-map-component.jsx";
import "./restaurant-info.scss";

export default function RestaurantInfo(props) {
  // This is something I use to make sure the drop down displays correctly
  // I've limited the app to only two columns so there will only be a right
  // or left, depending on the side its on I apply different css rules in
  // non mobile views
  const className =
    props.index % 2 === 0 ? "restaurant-info-l" : "restaurant-info-r";

  return (
    <div className={className}>
      <div className="restaurant-info-container">
        <div className="restaurant-info-map">
          <div className="map">
            <GoogleMapComponent location={props.location} />
          </div>
        </div>
        <div className="restaurant-info-item">
          <div className="details">
            <div className="details-contact">
              <div>{props.location.address}</div>
              <div>
                {props.location.city}, {props.location.state}{" "}
                {props.location.postalCode}
              </div>
              {/* {Not all restaurants give sufficient contact information} */}
              {props.contact ? (
                <div>
                  <div className="restaurant-phone">
                    {props.contact.formattedPhone}
                  </div>
                  {props.contact.twitter ? (
                    <div className="restaurant-twitter">
                      <a href={"https://twitter.com/" + props.contact.twitter}>
                        @{props.contact.twitter}
                      </a>
                    </div>
                  ) : null}
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
