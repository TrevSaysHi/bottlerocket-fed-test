import React, { useEffect, useState } from "react";
import RestaurantCard from "./restaurant-card/restaurant-card.jsx";
import Grid from "@material-ui/core/Grid";
import "./App.scss";

function App() {
  // response will contain the data fetched from the api
  const [response, setResponse] = useState([]);

  // Get the api information and set the state
  useEffect(() => {
    fetch("https://s3.amazonaws.com/br-codingexams/restaurants.json", {
      method: "GET"
    })
      .then(response => response.json())
      .then(data => {
        setResponse(data.restaurants);
      });
  }, []);

  return (
    <div className="App">
      <link
        href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap"
        rel="stylesheet"
      />
      <div className="header">Lunch Time</div>
      <Grid container className="restaurant-flex">
        {response.map((restaurant, index) => (
          <RestaurantCard
            name={restaurant.name}
            category={restaurant.category}
            imgSrc={restaurant.backgroundImageURL}
            location={restaurant.location}
            contact={restaurant.contact}
            key={index}
            index={index}
          ></RestaurantCard>
        ))}
      </Grid>
    </div>
  );
}

export default App;
