This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This project was initially made as an FED test, it was a great example of how to use hooks and interacting with 3rd party APIs such as google maps.

## Starting the Project

Run:

### `git clone {repo}`

### `npm install`

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

![image](https://i.imgur.com/ffV2woy.png)
![image](https://i.imgur.com/5NXAHiF.png)
